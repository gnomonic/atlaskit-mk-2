// French
export default {
  'fabric.media.retry': 'Réessayer',
  'fabric.media.failed_to_load': 'Échec du chargement',
  'fabric.media.recent_uploads': 'Importations récentes',
  'fabric.media.upload_file': 'Importer un fichier',
  'fabric.media.drag_and_drop_your_files':
    " Glisser-déplacer des fichiers n'importe où ou",
  'fabric.media.drop_your_files': 'Déposer vos fichiers à importer',
  'fabric.media.upload': 'Importer',
  'fabric.media.cancel': 'Annuler',
  'fabric.media.search_all_gifs': 'Rechercher parmi tous les GIF',
  'fabric.media.cant_retrieve_gifs':
    "Désolés, nous n'avons pu récupérer aucun GIF",
  'fabric.media.check_your_network': 'Vérifier votre connexion réseau',
  'fabric.media.try_again': 'Réessayez',
  'fabric.media.no_gifs_found': "Hello? Is it me you're looking for?",
  'fabric.media.no_gifs_found_suggestion':
    "Nous n'avons trouvé aucun résultat pour « {query} »",
  'fabric.media.load_more_gifs': 'Charger plus de GIF',
  'fabric.media.add_account': 'Ajouter un compte',
  'fabric.media.unlink_account': 'Dissocier le compte',
  'fabric.media.upload_file_from': 'Importer un fichier depuis {name}',
  'fabric.media.connect_to': 'Se connecter à {name}',
  'fabric.media.connect_account_description':
    'Nous ouvrirons une nouvelle page pour vous aider à vous connecter à votre compte {name}',
  'fabric.media.something_went_wrong': 'Something went wrong.',
  'fabric.media.might_be_a_hiccup': 'It might just be a hiccup.',
  'fabric.media.couldnt_generate_preview':
    "We couldn't generate a preview for this file.",
  'fabric.media.cant_preview_file_type': "We can't preview this file type.",
  'fabric.media.item_not_found_in_list':
    'The selected item was not found on the list.',
  'fabric.media.no_pdf_artifacts': 'No PDF artifacts found for this file.',
  'fabric.media.give_feedback': 'Give feedback',
  'fabric.media.try_downloading_file': 'Try downloading the file to view it.',
};
