// Dutch
export default {
  'fabric.media.retry': 'Probeer opnieuw',
  'fabric.media.failed_to_load': 'Fout bij het laden',
  'fabric.media.recent_uploads': 'Recente uploads',
  'fabric.media.upload_file': 'Bestand uploaden',
  'fabric.media.drag_and_drop_your_files':
    'Bestanden ergens naartoe slepen en neerzetten of',
  'fabric.media.drop_your_files': 'Zet hier je te uploaden bestanden neer',
  'fabric.media.upload': 'Uploaden',
  'fabric.media.cancel': 'Annuleren',
  'fabric.media.search_all_gifs': "Doorzoek alle GIF's!",
  'fabric.media.cant_retrieve_gifs': "Oei! We konden geen GIF's vinden",
  'fabric.media.check_your_network': 'Controleer je netwerkverbinding',
  'fabric.media.try_again': 'Opnieuw proberen',
  'fabric.media.no_gifs_found': 'Hallo? Was je naar mij op zoek?',
  'fabric.media.no_gifs_found_suggestion':
    'We konden niks vinden voor "{query}"',
  'fabric.media.load_more_gifs': "Meer GIF's laden",
  'fabric.media.add_account': 'Account toevoegen',
  'fabric.media.unlink_account': 'Verbinding met account verwijderen',
  'fabric.media.upload_file_from': 'Een bestand vanaf {name} uploaden',
  'fabric.media.connect_to': 'Met {name} verbinden',
  'fabric.media.connect_account_description':
    'We zullen een nieuwe pagina openen waarop je je {name}-account kunt verbinden',
  'fabric.media.something_went_wrong': 'Something went wrong.',
  'fabric.media.might_be_a_hiccup': 'It might just be a hiccup.',
  'fabric.media.couldnt_generate_preview':
    "We couldn't generate a preview for this file.",
  'fabric.media.cant_preview_file_type': "We can't preview this file type.",
  'fabric.media.item_not_found_in_list':
    'The selected item was not found on the list.',
  'fabric.media.no_pdf_artifacts': 'No PDF artifacts found for this file.',
  'fabric.media.give_feedback': 'Give feedback',
  'fabric.media.try_downloading_file': 'Try downloading the file to view it.',
};
