// Romanian
export default {
  'fabric.media.retry': 'Reîncercare',
  'fabric.media.failed_to_load': 'Încărcarea a eșuat',
  'fabric.media.recent_uploads': 'Încărcări recente',
  'fabric.media.upload_file': 'Încărcare fișier',
  'fabric.media.drag_and_drop_your_files':
    'Deplasați și plasați câmpurile oriunde sau',
  'fabric.media.drop_your_files': 'Fixați fișierele de încărcat',
  'fabric.media.upload': 'Încărcare',
  'fabric.media.cancel': 'Anulare',
  'fabric.media.search_all_gifs': 'Căutare în toate GIF-urile!',
  'fabric.media.cant_retrieve_gifs': 'Ups! Nu am putut obține niciun GIF!',
  'fabric.media.check_your_network': 'Verificați-vă conexiunea la rețea',
  'fabric.media.try_again': 'Încercați din nou',
  'fabric.media.no_gifs_found': 'Bună! Pe mine mă cauți?',
  'fabric.media.no_gifs_found_suggestion':
    'Nu am putut găsi nimic pentru „{query}”',
  'fabric.media.load_more_gifs': 'Încarcă mai multe GIF-uri',
  'fabric.media.add_account': 'Adăugare cont',
  'fabric.media.unlink_account': 'Anulare asociere Cont',
  'fabric.media.upload_file_from': 'Încărcare fișier de pe {name}',
  'fabric.media.connect_to': 'Conectare la {name}',
  'fabric.media.connect_account_description':
    'Vom deschide o pagină nouă pentru a vă ajuta să vă conectați contul {name}',
  'fabric.media.something_went_wrong': 'Something went wrong.',
  'fabric.media.might_be_a_hiccup': 'It might just be a hiccup.',
  'fabric.media.couldnt_generate_preview':
    "We couldn't generate a preview for this file.",
  'fabric.media.cant_preview_file_type': "We can't preview this file type.",
  'fabric.media.item_not_found_in_list':
    'The selected item was not found on the list.',
  'fabric.media.no_pdf_artifacts': 'No PDF artifacts found for this file.',
  'fabric.media.give_feedback': 'Give feedback',
  'fabric.media.try_downloading_file': 'Try downloading the file to view it.',
};
