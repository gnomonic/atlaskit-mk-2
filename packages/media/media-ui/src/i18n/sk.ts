// Slovak
export default {
  'fabric.media.retry': 'Opakovať',
  'fabric.media.failed_to_load': 'Nahratie zlyhalo',
  'fabric.media.recent_uploads': 'Posledné nahrania',
  'fabric.media.upload_file': 'Nahrať súbor',
  'fabric.media.drag_and_drop_your_files':
    'Kdekoľvek potiahnite a pustite súbory alebo',
  'fabric.media.drop_your_files': 'Pustite súbory na nahranie',
  'fabric.media.upload': 'Nahrať',
  'fabric.media.cancel': 'Zrušiť',
  'fabric.media.search_all_gifs': 'Prehľadať všetky obrázky GIF!',
  'fabric.media.cant_retrieve_gifs':
    'Ach nie! Nepodarilo sa nám získať žiadne obrázky GIF',
  'fabric.media.check_your_network': 'Skontrolujte sieťové pripojenie',
  'fabric.media.try_again': 'Skúste to znova',
  'fabric.media.no_gifs_found': 'Dobrý deň? Poviete nám, čo hľadáte?',
  'fabric.media.no_gifs_found_suggestion':
    'Nič sa nám nepodarilo nájsť pre "{query}"',
  'fabric.media.load_more_gifs': 'Načítať viac obrázkov GIF',
  'fabric.media.add_account': 'Pridať účet',
  'fabric.media.unlink_account': 'Zrušiť prepojenie účtu',
  'fabric.media.upload_file_from': 'Nahrať súbor z {name}',
  'fabric.media.connect_to': 'Pripojiť k {name}',
  'fabric.media.connect_account_description':
    'Otvoríme novú stránku, aby sme vám pomohli pri pripojení k vášmu účtu {name}',
  'fabric.media.something_went_wrong': 'Something went wrong.',
  'fabric.media.might_be_a_hiccup': 'It might just be a hiccup.',
  'fabric.media.couldnt_generate_preview':
    "We couldn't generate a preview for this file.",
  'fabric.media.cant_preview_file_type': "We can't preview this file type.",
  'fabric.media.item_not_found_in_list':
    'The selected item was not found on the list.',
  'fabric.media.no_pdf_artifacts': 'No PDF artifacts found for this file.',
  'fabric.media.give_feedback': 'Give feedback',
  'fabric.media.try_downloading_file': 'Try downloading the file to view it.',
};
