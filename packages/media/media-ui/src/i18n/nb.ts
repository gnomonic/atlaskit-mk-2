// Norwegian Bokmål
export default {
  'fabric.media.retry': 'Prøv igjen',
  'fabric.media.failed_to_load': 'Kunne ikke lastes inn',
  'fabric.media.recent_uploads': 'Nylige opplastinger',
  'fabric.media.upload_file': 'Last opp en fil',
  'fabric.media.drag_and_drop_your_files':
    'Dra og slipp filene dine hvor som helst eller',
  'fabric.media.drop_your_files': 'Slipp filer for å laste dem opp',
  'fabric.media.upload': 'Last opp',
  'fabric.media.cancel': 'Avbryt',
  'fabric.media.search_all_gifs': 'Søk i alle GIF-ene',
  'fabric.media.cant_retrieve_gifs':
    'Beklager. Vi kunne ikke hente noen GIF-bilder',
  'fabric.media.check_your_network': 'Kontroller nettverksforbindelsen',
  'fabric.media.try_again': 'Prøv på nytt',
  'fabric.media.no_gifs_found': 'Hei! Var det meg du lette etter?',
  'fabric.media.no_gifs_found_suggestion':
    'Vi kunne ikke finne noe for «{query}»',
  'fabric.media.load_more_gifs': 'Last opp flere GIF-bilder',
  'fabric.media.add_account': 'Legg til en konto',
  'fabric.media.unlink_account': 'Fjern kobling fra konto',
  'fabric.media.upload_file_from': 'Last opp en fil fra {name}',
  'fabric.media.connect_to': 'Koble til {name}',
  'fabric.media.connect_account_description':
    'Vi vil åpne en ny side for å hjelpe deg med å koble til {name}-kontoen din',
  'fabric.media.something_went_wrong': 'Something went wrong.',
  'fabric.media.might_be_a_hiccup': 'It might just be a hiccup.',
  'fabric.media.couldnt_generate_preview':
    "We couldn't generate a preview for this file.",
  'fabric.media.cant_preview_file_type': "We can't preview this file type.",
  'fabric.media.item_not_found_in_list':
    'The selected item was not found on the list.',
  'fabric.media.no_pdf_artifacts': 'No PDF artifacts found for this file.',
  'fabric.media.give_feedback': 'Give feedback',
  'fabric.media.try_downloading_file': 'Try downloading the file to view it.',
};
