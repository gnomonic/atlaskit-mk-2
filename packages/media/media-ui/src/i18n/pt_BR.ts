// Portuguese (Brazil)
export default {
  'fabric.media.retry': 'Tentar novamente',
  'fabric.media.failed_to_load': 'Falha ao carregar',
  'fabric.media.recent_uploads': 'Arquivos carregados recentemente',
  'fabric.media.upload_file': 'Carregar um arquivo',
  'fabric.media.drag_and_drop_your_files':
    'Arraste e solte seus arquivos em qualquer lugar ou',
  'fabric.media.drop_your_files': 'Solte seus arquivos para carregá-los',
  'fabric.media.upload': 'Carregar',
  'fabric.media.cancel': 'Cancelar',
  'fabric.media.search_all_gifs': 'Pesquise todos os GIFs!',
  'fabric.media.cant_retrieve_gifs':
    'Ops! Não foi possível encontrar nenhum GIF',
  'fabric.media.check_your_network': 'Verifique sua conexão de rede',
  'fabric.media.try_again': 'Tentar novamente',
  'fabric.media.no_gifs_found': 'Alô? Era isso que você estava procurando?',
  'fabric.media.no_gifs_found_suggestion':
    'Não encontramos nenhum resultado para "{query}"',
  'fabric.media.load_more_gifs': 'Carregar mais GIFs',
  'fabric.media.add_account': 'Adicionar conta',
  'fabric.media.unlink_account': 'Desvincular conta',
  'fabric.media.upload_file_from': 'Carregar um arquivo do {name}',
  'fabric.media.connect_to': 'Conectar-se ao {name}',
  'fabric.media.connect_account_description':
    'Abriremos uma nova página para ajudar você a conectar sua conta do {name}',
  'fabric.media.something_went_wrong': 'Something went wrong.',
  'fabric.media.might_be_a_hiccup': 'It might just be a hiccup.',
  'fabric.media.couldnt_generate_preview':
    "We couldn't generate a preview for this file.",
  'fabric.media.cant_preview_file_type': "We can't preview this file type.",
  'fabric.media.item_not_found_in_list':
    'The selected item was not found on the list.',
  'fabric.media.no_pdf_artifacts': 'No PDF artifacts found for this file.',
  'fabric.media.give_feedback': 'Give feedback',
  'fabric.media.try_downloading_file': 'Try downloading the file to view it.',
};
