// Estonian
export default {
  'fabric.media.retry': 'Proovi uuesti',
  'fabric.media.failed_to_load': 'Laadimine nurjus',
  'fabric.media.recent_uploads': 'Hiljutised üleslaadimised',
  'fabric.media.upload_file': 'Faili üleslaadimine',
  'fabric.media.drag_and_drop_your_files':
    'Lohista ja aseta oma failid ükskõik, kuhu või',
  'fabric.media.drop_your_files': 'Üleslaadimiseks aseta failid',
  'fabric.media.upload': 'Laadi üles',
  'fabric.media.cancel': 'Tühista',
  'fabric.media.search_all_gifs': 'Otsi kõikide GIFide seast!',
  'fabric.media.cant_retrieve_gifs': 'Oih! Me ei leidnud ühtki GIFi',
  'fabric.media.check_your_network': 'Kontrolli oma võrguühendust',
  'fabric.media.try_again': 'Proovi uuesti',
  'fabric.media.no_gifs_found': 'Tere.. Kas otsisid mind?',
  'fabric.media.no_gifs_found_suggestion':
    'Me ei leidnud seoses päringuga „{query}“ midagi',
  'fabric.media.load_more_gifs': 'Lae veel GIFe',
  'fabric.media.add_account': 'Lisa konto',
  'fabric.media.unlink_account': 'Eemalda kontolt seos',
  'fabric.media.upload_file_from': 'Laadi fail üles {name}-st',
  'fabric.media.connect_to': 'Ühendu teenusega {name}',
  'fabric.media.connect_account_description':
    '{name} konto ühendamisel abi pakkumiseks avame uue lehekülje',
  'fabric.media.something_went_wrong': 'Something went wrong.',
  'fabric.media.might_be_a_hiccup': 'It might just be a hiccup.',
  'fabric.media.couldnt_generate_preview':
    "We couldn't generate a preview for this file.",
  'fabric.media.cant_preview_file_type': "We can't preview this file type.",
  'fabric.media.item_not_found_in_list':
    'The selected item was not found on the list.',
  'fabric.media.no_pdf_artifacts': 'No PDF artifacts found for this file.',
  'fabric.media.give_feedback': 'Give feedback',
  'fabric.media.try_downloading_file': 'Try downloading the file to view it.',
};
