// Russian
export default {
  'fabric.media.retry': 'Повторить',
  'fabric.media.failed_to_load': 'Не удалось загрузить файл.',
  'fabric.media.recent_uploads': 'Недавно загруженные файлы',
  'fabric.media.upload_file': 'Загрузите файл',
  'fabric.media.drag_and_drop_your_files': 'Перетащите файлы в любое место или',
  'fabric.media.drop_your_files': 'Перетащите файлы для загрузки',
  'fabric.media.upload': 'Загрузить',
  'fabric.media.cancel': 'Отменить',
  'fabric.media.search_all_gifs': 'Поиск по всем файлам GIF',
  'fabric.media.cant_retrieve_gifs': 'Не найдено ни одного файла GIF',
  'fabric.media.check_your_network': 'Проверьте подключение к сети',
  'fabric.media.try_again': 'Повторить попытку',
  'fabric.media.no_gifs_found': 'Здравствуйте. Вы искали меня?',
  'fabric.media.no_gifs_found_suggestion':
    'По запросу «{query}» ничего не найдено',
  'fabric.media.load_more_gifs': 'Другие файлы GIF',
  'fabric.media.add_account': 'Добавить аккаунт',
  'fabric.media.unlink_account': 'Отменить связь с аккаунтом',
  'fabric.media.upload_file_from': 'Загрузить файл из {name}',
  'fabric.media.connect_to': 'Подключить к {name}',
  'fabric.media.connect_account_description':
    'Откроется новая страница, где вы сможете подключить свой аккаунт {name}',
  'fabric.media.something_went_wrong': 'Something went wrong.',
  'fabric.media.might_be_a_hiccup': 'It might just be a hiccup.',
  'fabric.media.couldnt_generate_preview':
    "We couldn't generate a preview for this file.",
  'fabric.media.cant_preview_file_type': "We can't preview this file type.",
  'fabric.media.item_not_found_in_list':
    'The selected item was not found on the list.',
  'fabric.media.no_pdf_artifacts': 'No PDF artifacts found for this file.',
  'fabric.media.give_feedback': 'Give feedback',
  'fabric.media.try_downloading_file': 'Try downloading the file to view it.',
};
