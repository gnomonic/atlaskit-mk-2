// German
export default {
  'fabric.media.retry': 'Erneut versuchen',
  'fabric.media.failed_to_load': 'Fehler beim Laden',
  'fabric.media.recent_uploads': 'Aktuelle Uploads',
  'fabric.media.upload_file': 'Datei hochladen',
  'fabric.media.drag_and_drop_your_files':
    'Dateien irgendwohin ziehen und ablegen oder',
  'fabric.media.drop_your_files': 'Dateien zum Hochladen ablegen',
  'fabric.media.upload': 'Hochladen',
  'fabric.media.cancel': 'Abbrechen',
  'fabric.media.search_all_gifs': 'Durchsuchen Sie alle GIFs!',
  'fabric.media.cant_retrieve_gifs':
    'Hoppla, es konnten keine GIFs abgerufen werden.',
  'fabric.media.check_your_network': 'Überprüfen Sie Ihre Netzwerkverbindung.',
  'fabric.media.try_again': 'Erneut versuchen',
  'fabric.media.no_gifs_found': 'Hallo? Haben Sie nach mir gesucht?',
  'fabric.media.no_gifs_found_suggestion':
    'Es wurden keine Ergebnisse für „{query}” gefunden.',
  'fabric.media.load_more_gifs': 'Mehr GIFs laden',
  'fabric.media.add_account': 'Account hinzufügen',
  'fabric.media.unlink_account': 'Account-Verknüpfung löschen',
  'fabric.media.upload_file_from': 'Datei von {name} hochladen',
  'fabric.media.connect_to': 'Mit {name} verbinden',
  'fabric.media.connect_account_description':
    'Es wird eine neue Seite geöffnet, auf der Sie Ihren {name}-Account verbinden können.',
  'fabric.media.something_went_wrong': 'Something went wrong.',
  'fabric.media.might_be_a_hiccup': 'It might just be a hiccup.',
  'fabric.media.couldnt_generate_preview':
    "We couldn't generate a preview for this file.",
  'fabric.media.cant_preview_file_type': "We can't preview this file type.",
  'fabric.media.item_not_found_in_list':
    'The selected item was not found on the list.',
  'fabric.media.no_pdf_artifacts': 'No PDF artifacts found for this file.',
  'fabric.media.give_feedback': 'Give feedback',
  'fabric.media.try_downloading_file': 'Try downloading the file to view it.',
};
