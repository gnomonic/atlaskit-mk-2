// Spanish
export default {
  'fabric.media.retry': 'Reintentar',
  'fabric.media.failed_to_load': 'No se pudo cargar',
  'fabric.media.recent_uploads': 'Cargas recientes',
  'fabric.media.upload_file': 'Cargar un archivo',
  'fabric.media.drag_and_drop_your_files':
    'Arrastra y suelta los archivos en cualquier lugar o',
  'fabric.media.drop_your_files': 'Suelta los archivos para cargarlos',
  'fabric.media.upload': 'Cargar',
  'fabric.media.cancel': 'Cancelar',
  'fabric.media.search_all_gifs': '¡Busca todos los GIF!',
  'fabric.media.cant_retrieve_gifs':
    '¡Qué pena! No hemos recuperado ningún GIF',
  'fabric.media.check_your_network': 'Comprueba tu conexión de red',
  'fabric.media.try_again': 'Volver a intentarlo',
  'fabric.media.no_gifs_found': 'Hola, ¿me buscabas?',
  'fabric.media.no_gifs_found_suggestion':
    'No hemos encontrado nada para "{query}"',
  'fabric.media.load_more_gifs': 'Cargar más GIF',
  'fabric.media.add_account': 'Añadir cuenta',
  'fabric.media.unlink_account': 'Desvincular cuenta',
  'fabric.media.upload_file_from': 'Carga un archivo de {name}',
  'fabric.media.connect_to': 'Conectarse a {name}',
  'fabric.media.connect_account_description':
    'Abriremos otra página para ayudarte a conectar tu cuenta de {name}',
  'fabric.media.something_went_wrong': 'Something went wrong.',
  'fabric.media.might_be_a_hiccup': 'It might just be a hiccup.',
  'fabric.media.couldnt_generate_preview':
    "We couldn't generate a preview for this file.",
  'fabric.media.cant_preview_file_type': "We can't preview this file type.",
  'fabric.media.item_not_found_in_list':
    'The selected item was not found on the list.',
  'fabric.media.no_pdf_artifacts': 'No PDF artifacts found for this file.',
  'fabric.media.give_feedback': 'Give feedback',
  'fabric.media.try_downloading_file': 'Try downloading the file to view it.',
};
