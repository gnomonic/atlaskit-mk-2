// English (United Kingdom)
export default {
  'fabric.media.retry': 'Retry',
  'fabric.media.failed_to_load': 'Failed to load',
  'fabric.media.recent_uploads': 'Recent uploads',
  'fabric.media.upload_file': 'Upload a file',
  'fabric.media.drag_and_drop_your_files':
    'Drag and drop your files anywhere or',
  'fabric.media.drop_your_files': 'Drop your files to upload',
  'fabric.media.upload': 'Upload',
  'fabric.media.cancel': 'Cancel',
  'fabric.media.search_all_gifs': 'Search all the GIFs!',
  'fabric.media.cant_retrieve_gifs': 'Ouch! We could not retrieve any GIFs',
  'fabric.media.check_your_network': 'Check your network connection',
  'fabric.media.try_again': 'Try again',
  'fabric.media.no_gifs_found': "Hello? Was it me you're looking for?",
  'fabric.media.no_gifs_found_suggestion':
    'We couldn\'t find anything for "{query}"',
  'fabric.media.load_more_gifs': 'Load more GIFs',
  'fabric.media.add_account': 'Add account',
  'fabric.media.unlink_account': 'Unlink Account',
  'fabric.media.upload_file_from': 'Upload a file from {name}',
  'fabric.media.connect_to': 'Connect to {name}',
  'fabric.media.connect_account_description':
    "We'll open a new page to help you connect your {name} account",
  'fabric.media.something_went_wrong': 'Something went wrong.',
  'fabric.media.might_be_a_hiccup': 'It might just be a hiccup.',
  'fabric.media.couldnt_generate_preview':
    "We couldn't generate a preview for this file.",
  'fabric.media.cant_preview_file_type': "We can't preview this file type.",
  'fabric.media.item_not_found_in_list':
    'The selected item was not found on the list.',
  'fabric.media.no_pdf_artifacts': 'No PDF artifacts found for this file.',
  'fabric.media.give_feedback': 'Give feedback',
  'fabric.media.try_downloading_file': 'Try downloading the file to view it.',
};
