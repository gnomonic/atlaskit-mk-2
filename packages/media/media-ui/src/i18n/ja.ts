// Japanese
export default {
  'fabric.media.retry': '再試行',
  'fabric.media.failed_to_load': '読み込めませんでした',
  'fabric.media.recent_uploads': '最近アップロードしたファイル',
  'fabric.media.upload_file': 'ファイルをアップロード',
  'fabric.media.drag_and_drop_your_files':
    'ファイルをここにドラッグ&ドロップ、または',
  'fabric.media.drop_your_files': 'アップロードするファイルをドロップ',
  'fabric.media.upload': 'アップロード',
  'fabric.media.cancel': 'キャンセル',
  'fabric.media.search_all_gifs': 'すべてのGIF画像を検索！',
  'fabric.media.cant_retrieve_gifs': 'GIF 画像を取得できませんでした',
  'fabric.media.check_your_network': 'ネットワーク接続を確認してください',
  'fabric.media.try_again': '再試行',
  'fabric.media.no_gifs_found':
    'もしもし？お探しの画像はこちらではありませんよね？',
  'fabric.media.no_gifs_found_suggestion':
    '「{query}」に一致する結果が見つかりませんでした',
  'fabric.media.load_more_gifs': 'さらに GIF を読み込む',
  'fabric.media.add_account': 'アカウントを追加',
  'fabric.media.unlink_account': 'アカウントのリンクを解除',
  'fabric.media.upload_file_from': '{name} からファイルをアップロード',
  'fabric.media.connect_to': '{name} に接続',
  'fabric.media.connect_account_description':
    '{name} アカウントを接続するため、新ページを開きます',
  'fabric.media.something_went_wrong': 'Something went wrong.',
  'fabric.media.might_be_a_hiccup': 'It might just be a hiccup.',
  'fabric.media.couldnt_generate_preview':
    "We couldn't generate a preview for this file.",
  'fabric.media.cant_preview_file_type': "We can't preview this file type.",
  'fabric.media.item_not_found_in_list':
    'The selected item was not found on the list.',
  'fabric.media.no_pdf_artifacts': 'No PDF artifacts found for this file.',
  'fabric.media.give_feedback': 'Give feedback',
  'fabric.media.try_downloading_file': 'Try downloading the file to view it.',
};
