// Swedish
export default {
  'fabric.media.retry': 'Försök igen',
  'fabric.media.failed_to_load': 'Fel vid inläsning',
  'fabric.media.recent_uploads': 'Senaste uppladdningar',
  'fabric.media.upload_file': 'Ladda upp en fil',
  'fabric.media.drag_and_drop_your_files':
    'Dra och släpp dina filer var som helst eller',
  'fabric.media.drop_your_files': 'Släpp dina filer för att ladda upp',
  'fabric.media.upload': 'Ladda upp',
  'fabric.media.cancel': 'Avbryt',
  'fabric.media.search_all_gifs': 'Sök alla GIF:ar!',
  'fabric.media.cant_retrieve_gifs': 'Oj då! Vi kunde inte hämta några GIF:ar',
  'fabric.media.check_your_network': 'Kontrollera din nätverksanslutning',
  'fabric.media.try_again': 'Försök igen',
  'fabric.media.no_gifs_found': 'Hello? Var det mig du letade efter?',
  'fabric.media.no_gifs_found_suggestion':
    'Vi kunde inte hitta några resultat för "{query}"',
  'fabric.media.load_more_gifs': 'Läs in fler GIF:ar',
  'fabric.media.add_account': 'Lägg till konto',
  'fabric.media.unlink_account': 'Ta bort kontolänk',
  'fabric.media.upload_file_from': 'Ladda upp en fil från {name}',
  'fabric.media.connect_to': 'Anslut till {name}',
  'fabric.media.connect_account_description':
    'Vi kommer att öppna en ny sida som hjälper dig att ansluta till ditt {name}-konto',
  'fabric.media.something_went_wrong': 'Something went wrong.',
  'fabric.media.might_be_a_hiccup': 'It might just be a hiccup.',
  'fabric.media.couldnt_generate_preview':
    "We couldn't generate a preview for this file.",
  'fabric.media.cant_preview_file_type': "We can't preview this file type.",
  'fabric.media.item_not_found_in_list':
    'The selected item was not found on the list.',
  'fabric.media.no_pdf_artifacts': 'No PDF artifacts found for this file.',
  'fabric.media.give_feedback': 'Give feedback',
  'fabric.media.try_downloading_file': 'Try downloading the file to view it.',
};
