// Hungarian
export default {
  'fabric.media.retry': 'Újra',
  'fabric.media.failed_to_load': 'A betöltés nem sikerült',
  'fabric.media.recent_uploads': 'Legutóbb feltöltések',
  'fabric.media.upload_file': 'Fájl feltöltése',
  'fabric.media.drag_and_drop_your_files':
    'Húzza és ejtse ide egy fájlokat vagy',
  'fabric.media.drop_your_files': 'Ejtse ide a fájlokat a feltöltéshez',
  'fabric.media.upload': 'Feltöltés',
  'fabric.media.cancel': 'Mégse',
  'fabric.media.search_all_gifs': 'Keresés az összes GIF között',
  'fabric.media.cant_retrieve_gifs':
    'Jaj! Nem sikerült lekérni egyetlen GIF-et sem',
  'fabric.media.check_your_network': 'Ellenőrizze a hálózati kapcsolatot',
  'fabric.media.try_again': 'Próbálja újra',
  'fabric.media.no_gifs_found': 'Hello? Engem keresett?',
  'fabric.media.no_gifs_found_suggestion':
    'Nem találtunk semmit sem a következő kifejezésre: {query}',
  'fabric.media.load_more_gifs': 'További GIF-ek betöltése',
  'fabric.media.add_account': 'Fiók hozzáadása',
  'fabric.media.unlink_account': 'Fiók leválasztása',
  'fabric.media.upload_file_from': 'Fájl feltöltése innen: {name}',
  'fabric.media.connect_to': 'Csatlakozás ehhez: {name}',
  'fabric.media.connect_account_description':
    'Megnyitunk egy új lapot, hogy könnyebben csatlakozhasson {name}-fiókjához',
  'fabric.media.something_went_wrong': 'Something went wrong.',
  'fabric.media.might_be_a_hiccup': 'It might just be a hiccup.',
  'fabric.media.couldnt_generate_preview':
    "We couldn't generate a preview for this file.",
  'fabric.media.cant_preview_file_type': "We can't preview this file type.",
  'fabric.media.item_not_found_in_list':
    'The selected item was not found on the list.',
  'fabric.media.no_pdf_artifacts': 'No PDF artifacts found for this file.',
  'fabric.media.give_feedback': 'Give feedback',
  'fabric.media.try_downloading_file': 'Try downloading the file to view it.',
};
