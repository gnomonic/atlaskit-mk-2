// Finnish
export default {
  'fabric.media.retry': 'Yritä uudelleen',
  'fabric.media.failed_to_load': 'Ei voitu ladata',
  'fabric.media.recent_uploads': 'Viimeaikaiset lataukset',
  'fabric.media.upload_file': 'Lataa tiedosto',
  'fabric.media.drag_and_drop_your_files':
    'Vedä ja pudota tiedostosi minne tahansa tai',
  'fabric.media.drop_your_files': 'Pudota tiedostosi ladattavaksi',
  'fabric.media.upload': 'Lataa',
  'fabric.media.cancel': 'Peruuta',
  'fabric.media.search_all_gifs': 'Hae kaikista GIFeistä!',
  'fabric.media.cant_retrieve_gifs': 'Voi ei! Emme voineet noutaa yhtään GIFiä',
  'fabric.media.check_your_network': 'Tarkista verkkoyhteytesi',
  'fabric.media.try_again': 'Yritä uudelleen',
  'fabric.media.no_gifs_found': 'Hei? Minuako etsit?',
  'fabric.media.no_gifs_found_suggestion':
    'Emme löytäneet tuloksia kyselylle "{query}"',
  'fabric.media.load_more_gifs': 'Lataa lisää GIFejä',
  'fabric.media.add_account': 'Lisää tili',
  'fabric.media.unlink_account': 'Poista tilin linkitys',
  'fabric.media.upload_file_from': 'Lataa tiedosto kohteesta {name}',
  'fabric.media.connect_to': 'Yhdistä kohteeseen {name}',
  'fabric.media.connect_account_description':
    'Avaamme uuden sivun, jotta voimme auttaa sinua yhdistämään {name} -tilisi',
  'fabric.media.something_went_wrong': 'Something went wrong.',
  'fabric.media.might_be_a_hiccup': 'It might just be a hiccup.',
  'fabric.media.couldnt_generate_preview':
    "We couldn't generate a preview for this file.",
  'fabric.media.cant_preview_file_type': "We can't preview this file type.",
  'fabric.media.item_not_found_in_list':
    'The selected item was not found on the list.',
  'fabric.media.no_pdf_artifacts': 'No PDF artifacts found for this file.',
  'fabric.media.give_feedback': 'Give feedback',
  'fabric.media.try_downloading_file': 'Try downloading the file to view it.',
};
