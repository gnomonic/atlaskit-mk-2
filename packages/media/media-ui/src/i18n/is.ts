// Icelandic
export default {
  'fabric.media.retry': 'Reyna aftur  ',
  'fabric.media.failed_to_load': 'Ekki tókst að hlaða',
  'fabric.media.recent_uploads': 'Nýlegar uppfærslur',
  'fabric.media.upload_file': 'Hlaða inn frá skrá',
  'fabric.media.drag_and_drop_your_files':
    'Dragðu og slepptu skránum þínum hvar sem er eða',
  'fabric.media.drop_your_files': 'Slepptu skránum þínum til að uppfæra þær',
  'fabric.media.upload': 'Hlaða upp',
  'fabric.media.cancel': 'Hætta við',
  'fabric.media.search_all_gifs': 'Leita í öllum GIF!',
  'fabric.media.cant_retrieve_gifs': 'Æi! Við gátum ekki sótt nein GIF',
  'fabric.media.check_your_network': 'Athugaðu nettenginguna þína',
  'fabric.media.try_again': 'Reyna aftur',
  'fabric.media.no_gifs_found': "Hello? Was it me you're looking for?",
  'fabric.media.no_gifs_found_suggestion':
    'Okkur tókst ekki að finna neitt fyrir "{query}"',
  'fabric.media.load_more_gifs': 'Hlaða meiru GIF',
  'fabric.media.add_account': 'Bæta við account',
  'fabric.media.unlink_account': 'Aftengjast account',
  'fabric.media.upload_file_from': 'Hladdu upp skrá frá {name}',
  'fabric.media.connect_to': 'Tengjast {name}',
  'fabric.media.connect_account_description':
    'Við munum opna nýja síðu til að hjálpa þér að tengja {name} reikninginn þinn',
  'fabric.media.something_went_wrong': 'Something went wrong.',
  'fabric.media.might_be_a_hiccup': 'It might just be a hiccup.',
  'fabric.media.couldnt_generate_preview':
    "We couldn't generate a preview for this file.",
  'fabric.media.cant_preview_file_type': "We can't preview this file type.",
  'fabric.media.item_not_found_in_list':
    'The selected item was not found on the list.',
  'fabric.media.no_pdf_artifacts': 'No PDF artifacts found for this file.',
  'fabric.media.give_feedback': 'Give feedback',
  'fabric.media.try_downloading_file': 'Try downloading the file to view it.',
};
