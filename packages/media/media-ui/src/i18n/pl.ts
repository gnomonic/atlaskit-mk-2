// Polish
export default {
  'fabric.media.retry': 'Ponów',
  'fabric.media.failed_to_load': 'Niepowodzenie wczytywania',
  'fabric.media.recent_uploads': 'Ostatnio dodane',
  'fabric.media.upload_file': 'Prześlij plik',
  'fabric.media.drag_and_drop_your_files':
    'Przeciągnij i upuść pliki w dowolne miejsce lub',
  'fabric.media.drop_your_files': 'Upuść pliki, aby je przesłać',
  'fabric.media.upload': 'Prześlij',
  'fabric.media.cancel': 'Anuluj',
  'fabric.media.search_all_gifs': 'Szukaj wszystkich plików GIF!',
  'fabric.media.cant_retrieve_gifs':
    'Ojej! Nie można pobrać żadnych plików GIF',
  'fabric.media.check_your_network': 'Sprawdź swoje połączenie sieciowe.',
  'fabric.media.try_again': 'Spróbuj ponownie',
  'fabric.media.no_gifs_found': 'Halo! Czy to mnie szukałeś?',
  'fabric.media.no_gifs_found_suggestion':
    'Nie udało się znaleźć wyników pasujących do zapytania „{query}”',
  'fabric.media.load_more_gifs': 'Wczytaj więcej plików GIF',
  'fabric.media.add_account': 'Dodaj konto',
  'fabric.media.unlink_account': 'Odłącz konto',
  'fabric.media.upload_file_from': 'Prześlij plik z {name}',
  'fabric.media.connect_to': 'Połącz z {name}',
  'fabric.media.connect_account_description':
    'Otworzymy nową stronę, aby pomóc Ci połączyć swoje konto {name}',
  'fabric.media.something_went_wrong': 'Something went wrong.',
  'fabric.media.might_be_a_hiccup': 'It might just be a hiccup.',
  'fabric.media.couldnt_generate_preview':
    "We couldn't generate a preview for this file.",
  'fabric.media.cant_preview_file_type': "We can't preview this file type.",
  'fabric.media.item_not_found_in_list':
    'The selected item was not found on the list.',
  'fabric.media.no_pdf_artifacts': 'No PDF artifacts found for this file.',
  'fabric.media.give_feedback': 'Give feedback',
  'fabric.media.try_downloading_file': 'Try downloading the file to view it.',
};
