// Czech
export default {
  'fabric.media.retry': 'Zkusit znovu',
  'fabric.media.failed_to_load': 'Nepodařilo se načíst ',
  'fabric.media.recent_uploads': 'Nedávno nahráno',
  'fabric.media.upload_file': 'Nahrát soubor',
  'fabric.media.drag_and_drop_your_files':
    'Přetáhněte své soubory kamkoli na stránku nebo',
  'fabric.media.drop_your_files': 'Chcete-li nahrát soubory, přetáhněte je sem',
  'fabric.media.upload': 'Nahrát',
  'fabric.media.cancel': 'Zrušit',
  'fabric.media.search_all_gifs': 'Prohledejte všechny GIFy!',
  'fabric.media.cant_retrieve_gifs': 'Nepodařilo se nám dohledat žádné GIFy.',
  'fabric.media.check_your_network': 'Zkontrolujte své připojení k síti.',
  'fabric.media.try_again': 'Zkusit znovu',
  'fabric.media.no_gifs_found': 'Haló? Hledali jste mě?',
  'fabric.media.no_gifs_found_suggestion':
    'Pro výraz „{query}“ jsme nic nenašli.',
  'fabric.media.load_more_gifs': 'Nahrát více GIFů',
  'fabric.media.add_account': 'Přidat účet',
  'fabric.media.unlink_account': 'Odpojit účet',
  'fabric.media.upload_file_from': 'Nahrát soubor z: {name}',
  'fabric.media.connect_to': 'Připojit k {name}',
  'fabric.media.connect_account_description':
    'Otevřeme novou stránku, abychom vám pomohli připojit váš {name} účet.',
  'fabric.media.something_went_wrong': 'Something went wrong.',
  'fabric.media.might_be_a_hiccup': 'It might just be a hiccup.',
  'fabric.media.couldnt_generate_preview':
    "We couldn't generate a preview for this file.",
  'fabric.media.cant_preview_file_type': "We can't preview this file type.",
  'fabric.media.item_not_found_in_list':
    'The selected item was not found on the list.',
  'fabric.media.no_pdf_artifacts': 'No PDF artifacts found for this file.',
  'fabric.media.give_feedback': 'Give feedback',
  'fabric.media.try_downloading_file': 'Try downloading the file to view it.',
};
